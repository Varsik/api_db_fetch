import React from 'react';
import {
  Route,
  Switch,
  BrowserRouter,
} from "react-router-dom";

import Home from "./components/home";
import List from "./components/list";
import Header from "./components/header";
import Footer from "./components/footer";
import ListItem from "./components/list-item";
import ErrorPage from "./components/error-page";
import Services from "./services";

import './App.css';


class App extends React.Component {
  state = {
    list: [],
    loading: true,
    currentPage: 1,
    postPerPage: 10,
  };

  services = new Services();

  componentDidMount() {
    this.services
      .getAllPost()
      .then((list) => {
        this.setState({
          list,
          loading: false,
        });
      });
      console.log(this.props)
  }
  addItem = (postElement) => {
    this.services
      .addPost(postElement)
      .then((body) => {
        console.log(body)
      });
  };

  deletePost = (item) => {
    this.services
      .deletePost(item.id)
      .then((body) => {
        console.log(body);
      });
  };

  render() {
    const { list, loading, currentPage, postPerPage} = this.state
    const indexOfLastPost = currentPage * postPerPage
    const lastOfFirstPost = indexOfLastPost - postPerPage
    const currentPosts = list.slice(lastOfFirstPost, indexOfLastPost)

    const totalPosts = list.length
    const pagitonLenght = ((totalPosts /postPerPage) - 1)
    const nextPaginate = (number) => {
      this.setState({ currentPage: (currentPage >  pagitonLenght) ? number = 1 :  number  })
    }
   const prevPaginate = (number) =>{    
      this.setState({ currentPage: (currentPage <= 1) ? number =  pagitonLenght + 1 :  number  })
      // console.log("number", number)
      // console.log("currentPage", currentPage)
      // console.log("currentPage", number)
   }
    const paginate = (number) => {
      this.setState({ currentPage:  number })
    }
  
    return (
      <div className="app">
        <BrowserRouter>
          <Header />
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/items" exact
              render={() =>
                <List
                  list={currentPosts}
                  loading={loading}
                  deletePost={this.deletePost}
                  addItem={this.addItem}
                  totalPosts={totalPosts}
                  postPerPage={postPerPage}
                  paginate={paginate}
                  nextPaginate={nextPaginate }
                  prevPaginate={prevPaginate}
                  acvitePganeNumber={currentPage}
                />}
            />
            {/* <Route path="/items/:itemId/" render={(props) => <ListItem item={item} loading={loading}  />}/>  */}
            <Route path="/items/:id/" component={ListItem} />
            <Route component={ErrorPage} />
          </Switch>
          <Footer />
        </BrowserRouter>
      </div>
    )
  }
}


export default App;
