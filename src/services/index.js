export default class Services {
	getResourse = async (url) => {
		let res = await fetch(url)
		return await res.json()
	};

	getAllPost = () => {
		return this.getResourse("https://jsonplaceholder.typicode.com/posts/")
	};

	getPost = (id) => {
		return this.getResourse(`https://jsonplaceholder.typicode.com/posts/${id}`)
	};

	deletePost = async (id) => {
		let response = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
			method: 'DELETE',
		});
		return await response.json();
	};
	
	addPost = async (post) => {
		let response = await fetch('https://jsonplaceholder.typicode.com/posts/', {
			method: 'POST',
			body: JSON.stringify(post),
			headers: {
				'Content-Type': 'application/json;charset=utf-8',
				'Accept': 'application/json',
			},
		});
		return await response.json();
	};
}
