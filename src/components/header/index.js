import React from "react";
import { NavLink } from "react-router-dom";

import "./index.css";

class Header extends React.Component {
  state = {
    fex: false
  };

  render() {
    return (
      <div>
        <nav >
          <NavLink 
            to="/"
            className="nav-link"
            activeClassName="active-Link"
          >
            home
          </NavLink>
          <ul>
            <li>
              <NavLink 
                to="/items"
                className="nav-link"
                activeClassName="active-Link"
              >
                items
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}

export default Header;
