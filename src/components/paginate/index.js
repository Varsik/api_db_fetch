import React from "react";
import { Link } from "react-router-dom";
import "./index.css"

// const  pageNumbers = []
// export default class Paginate extends React.Component {




//   render() {
//     for (let  i = 1; i <=  Math.ceil(this.props.totalPosts /this.props.postPerPage); i++) {
//       pageNumbers.push(i)
//     }


//     return (
//       <div>
//         <ul className="paginate">
//           {pageNumbers.map((number,item) => {
//             return (
//               <li>
//                 <Link to="#" className="link" key={number} onClick={()=>this.props.paginate(number)} > {number}</Link>
//               </li>
//             )
//           })}
//         </ul>
//       </div>
//     )
//   }
// }



const Paginate = ({ totalPosts,
  postPerPage,
  paginate,
  acvitePganeNumber,
  nextPaginate,
  prevPaginate }) => {

  const pageNumbers = []

  for (let i = 1; i <= Math.ceil(totalPosts / postPerPage); i++) {
    pageNumbers.push(i)
  }

  return (
    <div className="paginate">
      <span
        className="prev"
        onClick={() => prevPaginate(acvitePganeNumber - 1)}>
        <i className="fas fa-angle-left"></i>
      </span>
      <ul className="paginate-list">
        {pageNumbers.map((number) => {
          let avtiveClass = "link"
          avtiveClass = acvitePganeNumber === number ? avtiveClass += "  active" : avtiveClass += " "
          return (<li className={avtiveClass} key={number} >
            <Link to="#" onClick={() => paginate(number)}> {number}</Link >
          </li>)
        })}
      </ul>
      <span
        className="next"
        onClick={() => nextPaginate(acvitePganeNumber + 1)}>
        <i className="fas fa-angle-right"></i>
      </span>
    </div>
  )
};


export default Paginate;