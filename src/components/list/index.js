import React from "react";
import { Link } from "react-router-dom";

import Spinner from "../spinner";
import Paginate from "../paginate"
import AddItem from "../add-list"

import "./index.css";

const List = ({  list, 
                 loading,
                  deletePost,
                  totalPosts,
                  postPerPage,
                  paginate, 
                  acvitePganeNumber, 
                  nextPaginate, 
                  prevPaginate,
                  addItem 
                }) => {

    return (
      <div className="items">

        {loading ?
          <Spinner /> :
          <ul className="itemsList">
            {
              list.map(({ id, title }) => (
                <li key={id}>
                  <div>
                    {id}
                    <Link to={`/items/${id}`}>
                      ---{title}---
                    </Link>
                    <Link to={`/items/${id}`}>
                      {title}
                    </Link>
                  </div>
                  <div className="delete">
                    <button 
                      className="deleteButton"
                      onClick={() => deletePost(id)}
                    >
                      <i className="fas fa-trash-alt"></i>
                    </button>
                  </div>
                </li>
              ))
            }
          </ul>
        }
          <Paginate  
            totalPosts={totalPosts}
            postPerPage={postPerPage}
            paginate={paginate}
            acvitePganeNumber={acvitePganeNumber}
            nextPaginate ={nextPaginate} 
            prevPaginate={prevPaginate} 
            />
         <AddItem addItem={addItem} />
      </div>
    )
};
export default  List;
