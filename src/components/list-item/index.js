import React, { Component } from "react";

import Spinner from "../spinner";
import Services from "../../services";


export default class ListItem extends Component {
  
  state = {
    item: [],
    loading: true
  };

  services = new Services()

  componentDidMount() {
    this.services
    .getPost(this.props.match.params.id)
    .then((item) => {
      this.setState({
        item,
        loading: false
      })
    })
    console.log(this.props)
    // this.props.getItemId(this.match.params.id)   
    // console.log(this.props.match.params.id)
  }


  render() {
    const { loading, item } = this.state
    return (
      <div>
        {loading ?
          <Spinner /> :
          <ul className="itemsList">
            <li>    {item.id}    </li>
            <li>  {item.title}   </li>
            <li>  {item.body}   </li>
          </ul>}
      </div>
    )
  }
};
