import React from 'react';
import "./index.css"

const Spinner = () => (
  <div>
    <div className="lds-css ng-scope">
      <div className="lds-spinner">
        <div>
        </div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  </div>
);

export default Spinner;
