import React, { Component } from "react";


import "./index.css";


export default class AddList extends Component {

  state = {
    title: " ",
    body: " "
  };

  postItem = (event) => {
  event.preventDefault();
   const postElement = {
      body: this.state.body,
      title: this.state.title,
    };
    this.props.addItem(postElement)
    this.setState({ title: " ", body : ""});
  };


  render() {
    return (
      <div className="addItem">
        <form>
          <input
            value={this.state.title}
            onChange={(event) => {
              this.setState({
                 title: event.target.value,
              });
            }}
          />
            <input
            value={this.state.body}
           onChange={(event) => {
              this.setState({
                body: event.target.value,
              });
            }}
          />
          <button onClick={this.postItem}>
            submit
          </button>
        </form>
      </div>
    )
  }
}
